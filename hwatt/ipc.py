from enum import Enum


class IpcMsgType(Enum):
    """ Inter-Process Communication message type
    """
    detect_tools = 'detect_tools'
    tools_list = 'tool_lists'


class IpcMsg:
    """ Inter-Process Communication message container
    """

    def __init__(self, msg_type: IpcMsgType, data=None) -> None:
        self.type = msg_type
        self.data = data

    def __str__(self):
        return f'type: {self.type} data: {self.data}'
