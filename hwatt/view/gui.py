import logging
from logging.handlers import QueueHandler
from multiprocessing import Process, Queue, current_process

from ipc import IpcMsg, IpcMsgType

# QT stuff
from PySide6.QtWidgets import QApplication, QMainWindow
from view.win_main_ui import Ui_MainWindow


class View:
    """ view object
    """

    def __init__(self, verb, q_log, q_rx, q_tx):
        super(View, self).__init__()

        self.logger = logging.getLogger('view')
        self.logger.setLevel(verb)
        self.logger.addHandler(QueueHandler(q_log))

        self.q_rx = q_rx
        self.q_tx = q_tx

        self.logger.debug(f'created q_tx:{self.q_tx}, q_rx:{self.q_rx}')

    def process(self):
        """ Execution method for the gui
        """

        self.logger.info(f'started pid: {current_process().pid}')

        app = GuiApp(logger=self.logger)
        app.run()

        # execution stopping
        self.logger.info(f'stopped pid: {current_process().pid}')


class GuiApp(QApplication):
    """ QT application object
    """

    def __init__(self, logger):
        super(GuiApp, self).__init__()

        self.logger = logging.getLogger('gui app')
        self.logger.setLevel(logger.level)
        self.logger.handlers = logger.handlers

    def run(self):
        mainWin = GuiMainWin(logger=self.logger)
        self.logger.debug(f'main window object created')
        mainWin.show()
        self.logger.debug(f'main window showing')
        status = self.exec()
        self.logger.debug(f'main window execution stopped with code {status}')
        return status


class GuiMainWin(QMainWindow, Ui_MainWindow):
    """ QT mainwindow object
    """

    def __init__(self, logger):
        super(GuiMainWin, self).__init__()
        self.logger = logging.getLogger('gui mainwin')
        self.logger.setLevel(logger.level)
        self.logger.handlers = logger.handlers

        self.logger.debug(f'created')

        self.setupUi(self)
        self.logger.debug(f'ui setuped')
