# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'tool_dp700_dashboard.ui'
##
## Created by: Qt User Interface Compiler version 6.5.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractSpinBox, QApplication, QDial, QDoubleSpinBox,
    QFrame, QHBoxLayout, QLabel, QLayout,
    QPushButton, QSizePolicy, QSpacerItem, QVBoxLayout,
    QWidget)

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(370, 236)
        self.verticalLayout_3 = QVBoxLayout(Form)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.frame_V = QFrame(Form)
        self.frame_V.setObjectName(u"frame_V")
        self.frame_V.setMaximumSize(QSize(350, 70))
        self.frame_V.setFrameShape(QFrame.StyledPanel)
        self.frame_V.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.frame_V)
        self.horizontalLayout_4.setSpacing(6)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(3, 3, 3, 3)
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.dial_V = QDial(self.frame_V)
        self.dial_V.setObjectName(u"dial_V")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dial_V.sizePolicy().hasHeightForWidth())
        self.dial_V.setSizePolicy(sizePolicy)
        self.dial_V.setMinimumSize(QSize(50, 50))
        self.dial_V.setMaximumSize(QSize(50, 50))
        self.dial_V.setLayoutDirection(Qt.LeftToRight)
        self.dial_V.setMaximum(50)
        self.dial_V.setInvertedAppearance(False)
        self.dial_V.setInvertedControls(False)
        self.dial_V.setNotchTarget(5.000000000000000)
        self.dial_V.setNotchesVisible(True)

        self.horizontalLayout_3.addWidget(self.dial_V)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label_targetV = QLabel(self.frame_V)
        self.label_targetV.setObjectName(u"label_targetV")
        self.label_targetV.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.label_targetV)

        self.doubleSpinBox_targetV = QDoubleSpinBox(self.frame_V)
        self.doubleSpinBox_targetV.setObjectName(u"doubleSpinBox_targetV")
        self.doubleSpinBox_targetV.setDecimals(3)
        self.doubleSpinBox_targetV.setValue(50.000000000000000)

        self.horizontalLayout.addWidget(self.doubleSpinBox_targetV)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label_OVP = QLabel(self.frame_V)
        self.label_OVP.setObjectName(u"label_OVP")
        self.label_OVP.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_2.addWidget(self.label_OVP)

        self.doubleSpinBox_OVP = QDoubleSpinBox(self.frame_V)
        self.doubleSpinBox_OVP.setObjectName(u"doubleSpinBox_OVP")
        self.doubleSpinBox_OVP.setDecimals(3)
        self.doubleSpinBox_OVP.setValue(50.000000000000000)

        self.horizontalLayout_2.addWidget(self.doubleSpinBox_OVP)


        self.verticalLayout.addLayout(self.horizontalLayout_2)


        self.horizontalLayout_3.addLayout(self.verticalLayout)

        self.doubleSpinBox_curV = QDoubleSpinBox(self.frame_V)
        self.doubleSpinBox_curV.setObjectName(u"doubleSpinBox_curV")
        self.doubleSpinBox_curV.setMinimumSize(QSize(110, 0))
        font = QFont()
        font.setPointSize(22)
        font.setBold(True)
        font.setItalic(False)
        self.doubleSpinBox_curV.setFont(font)
        self.doubleSpinBox_curV.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.doubleSpinBox_curV.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.doubleSpinBox_curV.setKeyboardTracking(False)
        self.doubleSpinBox_curV.setDecimals(3)
        self.doubleSpinBox_curV.setSingleStep(1.000000000000000)
        self.doubleSpinBox_curV.setValue(50.000000000000000)

        self.horizontalLayout_3.addWidget(self.doubleSpinBox_curV)

        self.label_V = QLabel(self.frame_V)
        self.label_V.setObjectName(u"label_V")
        font1 = QFont()
        font1.setPointSize(24)
        self.label_V.setFont(font1)

        self.horizontalLayout_3.addWidget(self.label_V)


        self.horizontalLayout_4.addLayout(self.horizontalLayout_3)


        self.verticalLayout_3.addWidget(self.frame_V)

        self.frame_A = QFrame(Form)
        self.frame_A.setObjectName(u"frame_A")
        self.frame_A.setMaximumSize(QSize(350, 70))
        self.frame_A.setFrameShape(QFrame.StyledPanel)
        self.frame_A.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_5 = QHBoxLayout(self.frame_A)
        self.horizontalLayout_5.setSpacing(6)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(3, 3, 3, 3)
        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalLayout_6.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.dial_A = QDial(self.frame_A)
        self.dial_A.setObjectName(u"dial_A")
        sizePolicy.setHeightForWidth(self.dial_A.sizePolicy().hasHeightForWidth())
        self.dial_A.setSizePolicy(sizePolicy)
        self.dial_A.setMinimumSize(QSize(50, 50))
        self.dial_A.setMaximumSize(QSize(50, 50))
        self.dial_A.setLayoutDirection(Qt.LeftToRight)
        self.dial_A.setMaximum(5)
        self.dial_A.setInvertedAppearance(False)
        self.dial_A.setInvertedControls(False)
        self.dial_A.setNotchTarget(1.000000000000000)
        self.dial_A.setNotchesVisible(True)

        self.horizontalLayout_6.addWidget(self.dial_A)

        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.label_targetA = QLabel(self.frame_A)
        self.label_targetA.setObjectName(u"label_targetA")
        self.label_targetA.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_7.addWidget(self.label_targetA)

        self.doubleSpinBox_targetA = QDoubleSpinBox(self.frame_A)
        self.doubleSpinBox_targetA.setObjectName(u"doubleSpinBox_targetA")
        self.doubleSpinBox_targetA.setDecimals(3)
        self.doubleSpinBox_targetA.setValue(5.000000000000000)

        self.horizontalLayout_7.addWidget(self.doubleSpinBox_targetA)


        self.verticalLayout_2.addLayout(self.horizontalLayout_7)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.label_OVC = QLabel(self.frame_A)
        self.label_OVC.setObjectName(u"label_OVC")
        self.label_OVC.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_8.addWidget(self.label_OVC)

        self.doubleSpinBox_OVC = QDoubleSpinBox(self.frame_A)
        self.doubleSpinBox_OVC.setObjectName(u"doubleSpinBox_OVC")
        self.doubleSpinBox_OVC.setDecimals(3)
        self.doubleSpinBox_OVC.setValue(5.000000000000000)

        self.horizontalLayout_8.addWidget(self.doubleSpinBox_OVC)


        self.verticalLayout_2.addLayout(self.horizontalLayout_8)


        self.horizontalLayout_6.addLayout(self.verticalLayout_2)

        self.doubleSpinBox_curA = QDoubleSpinBox(self.frame_A)
        self.doubleSpinBox_curA.setObjectName(u"doubleSpinBox_curA")
        self.doubleSpinBox_curA.setMinimumSize(QSize(110, 0))
        self.doubleSpinBox_curA.setFont(font)
        self.doubleSpinBox_curA.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.doubleSpinBox_curA.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.doubleSpinBox_curA.setKeyboardTracking(False)
        self.doubleSpinBox_curA.setDecimals(3)
        self.doubleSpinBox_curA.setSingleStep(1.000000000000000)
        self.doubleSpinBox_curA.setValue(5.000000000000000)

        self.horizontalLayout_6.addWidget(self.doubleSpinBox_curA)

        self.label_A = QLabel(self.frame_A)
        self.label_A.setObjectName(u"label_A")
        self.label_A.setFont(font1)

        self.horizontalLayout_6.addWidget(self.label_A)


        self.horizontalLayout_5.addLayout(self.horizontalLayout_6)


        self.verticalLayout_3.addWidget(self.frame_A)

        self.frame_W = QFrame(Form)
        self.frame_W.setObjectName(u"frame_W")
        self.frame_W.setMaximumSize(QSize(350, 70))
        self.frame_W.setFrameShape(QFrame.StyledPanel)
        self.frame_W.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_9 = QHBoxLayout(self.frame_W)
        self.horizontalLayout_9.setSpacing(6)
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.horizontalLayout_9.setContentsMargins(3, 3, 3, 3)
        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.horizontalLayout_10.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.btn_OUTPUT = QPushButton(self.frame_W)
        self.btn_OUTPUT.setObjectName(u"btn_OUTPUT")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.btn_OUTPUT.sizePolicy().hasHeightForWidth())
        self.btn_OUTPUT.setSizePolicy(sizePolicy1)
        self.btn_OUTPUT.setMinimumSize(QSize(50, 50))
        self.btn_OUTPUT.setCheckable(True)
        self.btn_OUTPUT.setAutoExclusive(False)
        self.btn_OUTPUT.setFlat(False)

        self.horizontalLayout_10.addWidget(self.btn_OUTPUT)

        self.btn_APPLY = QPushButton(self.frame_W)
        self.btn_APPLY.setObjectName(u"btn_APPLY")
        self.btn_APPLY.setMinimumSize(QSize(50, 50))

        self.horizontalLayout_10.addWidget(self.btn_APPLY)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_10.addItem(self.horizontalSpacer)

        self.doubleSpinBox_curW = QDoubleSpinBox(self.frame_W)
        self.doubleSpinBox_curW.setObjectName(u"doubleSpinBox_curW")
        self.doubleSpinBox_curW.setMinimumSize(QSize(110, 0))
        self.doubleSpinBox_curW.setFont(font)
        self.doubleSpinBox_curW.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.doubleSpinBox_curW.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.doubleSpinBox_curW.setKeyboardTracking(False)
        self.doubleSpinBox_curW.setDecimals(3)
        self.doubleSpinBox_curW.setMaximum(200.000000000000000)
        self.doubleSpinBox_curW.setSingleStep(1.000000000000000)
        self.doubleSpinBox_curW.setValue(200.000000000000000)

        self.horizontalLayout_10.addWidget(self.doubleSpinBox_curW)

        self.label_W = QLabel(self.frame_W)
        self.label_W.setObjectName(u"label_W")
        self.label_W.setFont(font1)

        self.horizontalLayout_10.addWidget(self.label_W)


        self.horizontalLayout_9.addLayout(self.horizontalLayout_10)


        self.verticalLayout_3.addWidget(self.frame_W)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
#if QT_CONFIG(tooltip)
        self.dial_V.setToolTip(QCoreApplication.translate("Form", u"Set target voltage", None))
#endif // QT_CONFIG(tooltip)
        self.label_targetV.setText(QCoreApplication.translate("Form", u"Target", None))
#if QT_CONFIG(tooltip)
        self.doubleSpinBox_targetV.setToolTip(QCoreApplication.translate("Form", u"Set target voltage", None))
#endif // QT_CONFIG(tooltip)
        self.label_OVP.setText(QCoreApplication.translate("Form", u"OVP", None))
#if QT_CONFIG(tooltip)
        self.doubleSpinBox_OVP.setToolTip(QCoreApplication.translate("Form", u"Over-Voltage Protection threshold", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        self.doubleSpinBox_curV.setToolTip(QCoreApplication.translate("Form", u"Measured Voltage", None))
#endif // QT_CONFIG(tooltip)
        self.label_V.setText(QCoreApplication.translate("Form", u"V", None))
#if QT_CONFIG(tooltip)
        self.dial_A.setToolTip(QCoreApplication.translate("Form", u"Set current target", None))
#endif // QT_CONFIG(tooltip)
        self.label_targetA.setText(QCoreApplication.translate("Form", u"Target", None))
#if QT_CONFIG(tooltip)
        self.doubleSpinBox_targetA.setToolTip(QCoreApplication.translate("Form", u"Set target current", None))
#endif // QT_CONFIG(tooltip)
        self.label_OVC.setText(QCoreApplication.translate("Form", u"OCP", None))
#if QT_CONFIG(tooltip)
        self.doubleSpinBox_OVC.setToolTip(QCoreApplication.translate("Form", u"Over-Current Protection threshold", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        self.doubleSpinBox_curA.setToolTip(QCoreApplication.translate("Form", u"Measured Current", None))
#endif // QT_CONFIG(tooltip)
        self.label_A.setText(QCoreApplication.translate("Form", u"A", None))
#if QT_CONFIG(tooltip)
        self.btn_OUTPUT.setToolTip(QCoreApplication.translate("Form", u"Enable/Disable Output", None))
#endif // QT_CONFIG(tooltip)
        self.btn_OUTPUT.setText(QCoreApplication.translate("Form", u"OUTPUT", None))
        self.btn_APPLY.setText(QCoreApplication.translate("Form", u"APPLY", None))
#if QT_CONFIG(tooltip)
        self.doubleSpinBox_curW.setToolTip(QCoreApplication.translate("Form", u"Measured Power", None))
#endif // QT_CONFIG(tooltip)
        self.label_W.setText(QCoreApplication.translate("Form", u"W", None))
    # retranslateUi

