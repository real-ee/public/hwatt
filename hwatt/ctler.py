import logging
from logging.handlers import QueueHandler
from multiprocessing import Process, Queue, current_process
from queue import Empty

from ipc import IpcMsg, IpcMsgType


class Ctler:

    # instance attributes
    def __init__(self, verb, q_log, q_rx, q_v, q_m):
        super(Ctler, self).__init__()

        self.logger = logging.getLogger('ctler')
        self.logger.setLevel(verb)
        self.logger.addHandler(QueueHandler(q_log))

        self.q_rx = q_rx
        self.q_v = q_v
        self.q_m = q_m

        self.logger.debug(f'created q_rx:{self.q_rx}, q_m:{self.q_m}, q_v:{self.q_v}')

    def process(self):
        self.logger.info(f'started pid: {current_process().pid}')

        # execution loop
        while True:
            try:
                msg = self.q_rx.get(timeout=0.01)
            except Empty:
                continue

            # stop execution (only view can request that)
            if msg is None:
                break
            else:
                self.logger.debug(f'msg rx {msg}')
                # detect tools requested
                if msg.type == IpcMsgType.detect_tools:
                    self.logger.debug(f'forwarding to model {msg}')
                    self.q_m.put(msg)  # forward the detection request to model
                elif msg.type == IpcMsgType.tools_list:
                    self.logger.debug(f'forwarding to view {msg}')
                    self.q_v.put(msg)  # forward the tool list to view
                else:
                    self.logger.warning(f'unrecognized msg: {msg}')

        # execution stopping
        self.logger.info(f'stopped pid: {current_process().pid}')
